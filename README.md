Simple template that can init requirejs config
-----------------------------------------

## Installation

```
npm install grunt-template-simple-requirejs --save-dev
```

## Sample configuration
```
module.exports = function (grunt) {
    grunt.initConfig({
        // some metadata to be used in other tasks
        meta : {
            package: grunt.file.readJSON('package.json'),
            // Specify where our test files are.  Once we get all the tests switched over we can use 'test/js/**/*.spec.js' to automatically load all tests.
            specs : ['test/**/*.js'],
            coverage: {
                folder: '.generated-test/coverage'
            },
            src: ['src/**/*.js']
        },

        // The clean task ensures all files are removed from the dist/ directory so
        // that no files linger from previous builds.
        clean: {
            dist: ["dist/"],
            test: [".generated-test"]
        },

        /*
        Run a server on a specific port to serve assets from home folder. Proxies can be set to forward certain
        host:port's to a subpath.
         */
        serverwithproxy: {
            //we need to run a file server while running jasmine tests.
            test: {
                port: 6780,
                home: "",
                proxies: {}
            }
        },

        /**
         * Copy files from one folder to another
         */
        copy: {
            coverage: {
                files: [
                    {src: ["assets/**"], dest: "<%= meta.coverage.folder %>/", expand: true},
                    {src: ["src/components/**"], dest: "<%= meta.coverage.folder %>/", expand: true}
                ]
            }
        },

        /*
         Execute jasmine tests using a runner html file.
         */
        jasmine: {
            options: {
                specs: '<%= meta.specs %>',
                templateOptions: {
                    requirejs: 'src/components/requirejs/require.js',
                    requirejsConfigFile: 'config.js',
                    requirejsConfig: {}
                },
                template: require('grunt-template-simple-requirejs'),
                host : 'http://127.0.0.1:6780/',
                junit: {
                    path: ".generated-test/results",
                    consolidate: true
                },
                helpers: [
                    'src/components/jquery/jquery.js',
                    'assets/test/sinon-1.6.0.js',
                    'assets/test/jasmine.async.js',
                    'assets/test/jasmine-sinon.js',
                    'assets/test/jasmine-jquery.js'
                ]
            },

            // jasmine:test runs with the default settings
            test : {

            },

            // jasmine:coverage will first instrument the code to calculate code coverage
            coverage : {
                options: {
                    //the tests to be instrumented
                    specs: '<%= meta.specs %>',

                    //this template will set up things for istanbul.
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        //output with coverage information
                        coverage: '<%= meta.coverage.folder %>/coverage.json',

                        //we use our own template as the base, so we can also setup requirejs path
                        template: require('grunt-template-simple-requirejs'),
                        templateOptions: {
                            requirejs: 'src/components/requirejs/require.js',
                            requirejsConfigFile: 'config.js',
                            requirejsConfig: {
                                baseUrl: './.generated-test/coverage/src/'
                            }
                        },

                        //used by grunt-template-jasmine-isanbul to create output reports
                        report: [
                            {
                                type: 'html',
                                options: {
                                    dir: '<%= meta.coverage.folder %>/html'
                                }
                            },
                            {
                                type: 'cobertura',
                                options: {
                                    dir: '<%= meta.coverage.folder %>/cobertura'
                                }
                            }
                        ]
                    },
                    helpers: [
                        'src/components/jquery/jquery.js',
                        'assets/test/sinon-1.6.0.js',
                        'assets/test/jasmine.async.js',
                        'assets/test/jasmine-sinon.js',
                        'assets/test/jasmine-jquery.js',
                        'js/coverage_helper.js'
                    ]
                }
            }
        },


        // Instrument the files for coverage
        instrument : {
            files: ['src/**/*.js', '!src/components/**/*.js'],
            options : {
                basePath : '<%= meta.coverage.folder %>'
            }
        },
        storeCoverage : {
            options : {
                dir : '<%= meta.coverage.folder %>'
            }
        },
        makeReport : {
            src : '<%= meta.coverage.folder %>/*.json',
            options : {
                type : 'lcov',
                dir : '<%= meta.coverage.folder %>'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    //These are custom Limecraft grunt tasks
    grunt.loadNpmTasks('grunt-serverwithproxy');

    //istanbul is used for test coverage (how much source code is covered by unit tests?)
    grunt.loadNpmTasks('grunt-istanbul');

    // Instrument JS and run tests to get a coverage report
    grunt.registerTask('coverage', ['clean:test', 'instrument', 'copy:coverage', 'jasmine:coverage', 'storeCoverage', 'makeReport']);

    // needed to make grunt-istanbul storeReport
    grunt.event.on('jasmine.coverage', function (coverage) {
        console.log("setting global __coverage__ var");
        global.__coverage__ = coverage;
    });
};

```